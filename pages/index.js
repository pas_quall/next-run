import React from 'react'
import Head from 'next/head'
import "../assets/scss/styles.scss"
import Header from '../components/base/header'
import Footer from '../components/base/footer'
import Text from '../components/modules/text'
import Infoaccordions from '../components/modules/infoaccordions'
import Imagegallery from '../components/modules/imagegallery'

class Home extends React.Component {

    render() {
        return (
            <>
                <Head>
                    <title>Startseite</title>
                    <link rel='icon' href='/icons/favicon.ico' />
                </Head>

                <Header/>

                <main className="main">

                    <Infoaccordions/>
                    <Text/>
                    <Imagegallery/>

                    <div className={'testgrid'}>
                        <div className={'gridWrap'}>
                            <div className={'w12'}>w12</div>
                            <div className={'w6'}>w6</div>
                            <div className={'w6'}>w6</div>
                            <div className={'w4'}>w4</div>
                            <div className={'w4'}>w4</div>
                            <div className={'w4'}>w4</div>
                            <div className={'w6 w-left-6'}>w4</div>
                        </div>
                    </div>

                </main>

                <Footer/>

            </>
        )

    }
}

export default Home;