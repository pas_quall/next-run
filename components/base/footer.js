import React from 'react'

class Footer extends React.Component {

    render() {
        return (

            <footer className='footer'>

                <div className={'gridWrap'}>

                    <div className={'w12'}>
                        copyright 2019
                    </div>

                </div>

            </footer>

        );
    }
}

export default Footer
