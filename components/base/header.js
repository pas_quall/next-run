import React from 'react'
import Link from 'next/link'

const Header = () => (

    <header className='header'>

        <div className={'gridWrap'}>

            <div className={'w12'}>
                <Link href={'/'}>
                    <a className='header-logo'>Next-Run-Starter</a>
                </Link>

                <nav>
                    <ul>
                        <li>
                            <Link href='/'>
                                <a>Startseite</a>
                            </Link>
                        </li>
                    </ul>
                </nav>

            </div>

        </div>

    </header>

);

export default Header
