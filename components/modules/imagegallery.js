import React from 'react'
import Swiper from 'react-id-swiper';

class Text extends React.Component {
    render() {

        const SwiperOptions = {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        };

        return (

            <div className={'imagegallery'}>

                <div className={'gridWrap'}>

                    <div className={'w12'}>

                        <h2>Slider:</h2>

                        <Swiper {...SwiperOptions}>
                            <div>
                                <figure>
                                    <picture>
                                        <source media="(min-width: 768px)" srcSet="https://via.placeholder.com/1920x500"/>
                                        <source srcSet="https://via.placeholder.com/1920x500"/>
                                        <img src="https://via.placeholder.com/1920x500" width={'100%'}  alt={'alt'}/>
                                    </picture>
                                </figure>
                            </div>
                            <div>
                                <figure>
                                    <picture>
                                        <source media="(min-width: 768px)" srcSet="https://via.placeholder.com/1920x500"/>
                                        <source srcSet="https://via.placeholder.com/1920x500"/>
                                        <img src="https://via.placeholder.com/1920x500" width={'100%'}  alt={'alt'}/>
                                    </picture>
                                </figure>
                            </div>
                            <div>
                                <figure>
                                    <picture>
                                        <source media="(min-width: 768px)" srcSet="https://via.placeholder.com/1920x500"/>
                                        <source srcSet="https://via.placeholder.com/1920x500"/>
                                        <img src="https://via.placeholder.com/1920x500" width={'100%'}  alt={'alt'}/>
                                    </picture>
                                </figure>
                            </div>
                            <div>
                                <figure>
                                    <picture>
                                        <source media="(min-width: 768px)" srcSet="https://via.placeholder.com/1920x500"/>
                                        <source srcSet="https://via.placeholder.com/1920x500"/>
                                        <img src="https://via.placeholder.com/1920x500" width={'100%'}  alt={'alt'}/>
                                    </picture>
                                </figure>
                            </div>
                            <div>
                                <figure>
                                    <picture>
                                        <source media="(min-width: 768px)" srcSet="https://via.placeholder.com/1920x500"/>
                                        <source srcSet="https://via.placeholder.com/1920x500"/>
                                        <img src="https://via.placeholder.com/1920x500" width={'100%'}  alt={'alt'}/>
                                    </picture>
                                </figure>
                            </div>
                        </Swiper>

                    </div>

                </div>

            </div>

        );
    }
}

export default Text