import React from 'react'
import Accordion from '../partials/accordion'

class Infoaccordions extends React.Component {

    state = {
        activeAccordionId: 0
    };

    changeActiveAccordionId = (id) => {

        this.setState({
            activeAccordionId: this.state.activeAccordionId === id ? -1 : id
        });

    };

    checkIfAccordionActive = (id) => {

        if(id === this.state.activeAccordionId){
            return true;
        }

    };

    render() {
        return (

            <div className="infoaccordions">

                <div className={'gridWrap'}>

                    <div className={'w12'}>

                        <h2>Accordions:</h2>

                        <Accordion accordionId={0} changeActiveAccordionId={this.changeActiveAccordionId} isActive={this.checkIfAccordionActive(0)} />
                        <Accordion accordionId={1} changeActiveAccordionId={this.changeActiveAccordionId} isActive={this.checkIfAccordionActive(1)} />
                        <Accordion accordionId={2} changeActiveAccordionId={this.changeActiveAccordionId} isActive={this.checkIfAccordionActive(2)} />

                    </div>

                </div>

            </div>

        );
    }
}

export default Infoaccordions