import React from 'react'
import ReactDOM from 'react-dom'

class Accordion extends React.Component {

    calcCollapsedHeight(){

        const node = ReactDOM.findDOMNode(this);

        const $accordion = node.querySelector('.accordion-content');
        const collapsedHeight = $accordion.scrollHeight + 'px';

        this.applyCollapsedHeight($accordion, collapsedHeight);

    };

    applyCollapsedHeight = ($accordion, collapsedHeight) => {
        $accordion.style.height = collapsedHeight;
    };

    componentDidMount(){

        this.calcCollapsedHeight();

    }

    render() {

        const { isActive, accordionId, changeActiveAccordionId } = this.props;

        return (
            <div className={'accordion ' + (isActive ? 'active' : '')}>
                <button onClick={changeActiveAccordionId.bind(this, accordionId)} className='accordion-trigger'>Accordion Trigger</button>
                <div className='accordion-content'>
                    Das ist der Text zum Accordion <br />
                    Das ist der Text zum Accordion
                </div>
            </div>
        );

    }
}

export default Accordion